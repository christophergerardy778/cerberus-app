import Vue from 'vue';
import router from '../../router';

export async function Login({ commit }, credentials) {
    try {
        const response = await Vue.axios.post("/login", credentials);

        if (response.data.ok) {
            const token = response.data.data;
            commit("Auth/setToken", token, { root: true });
            commit("User/setUser", token, { root: true });
            commit("User/setLogin", true, { root: true });
            router.push("/home");
        }

        return response.data;
    } catch (e) {
        return e;
    }
}

export async function createAccount({ commit }, user) {
    try {
        const response = await Vue.axios.post("/register", user);

        if (response.data.ok) {
            const token = response.data.data;
            commit("Auth/setToken", token, { root: true });
            commit("User/setUser", token, { root: true });
            commit("User/setLogin", true, { root: true });
            router.push("/home");
        }

        return response.data;

    } catch (e) {
        return e;
    }
}