import Vue from 'vue';

export async function createGroup({ commit }, data) {
    try {
        const { id, name, token } = data;

        const response = await Vue.axios.post("/create-group", { name, userId: id },
            { headers: { "Authorization": token } }
        );

        return response.data;

    } catch (e) {
        return e;
    }
}

export async function getAllGroups({ commit }, data) {
    try {
        const { id, token } = data;

        const response = await Vue.axios.get(`/my-groups/${id}`, {
            headers: { "Authorization": token }
        });

        commit("Groups/setGroups", response.data.data, { root: true });

    } catch (e) {
        return e;
    }
}

export async function getPublicationsAndInfoGroup({ commit }, data) {
    try {
        const { id, token } = data;

        const response = await Vue.axios.get(`/publications/${id}`, {
            headers: { "Authorization": token }
        });

        commit("Groups/setGroupAndPublications", response.data.data, { root: true });

    } catch (e) {
        return e;
    }
}

export async function createPublicacion({ commit }, Data) {
    try {
        const { data, token } = Data;

        const response = await Vue.axios.post("/publications/create", data, {
            headers: { "Authorization": token }
        });

        return response.data;

    } catch (e) {
        return e;
    }
}

export async function getGroup({ commit }, data) {
    try {
        const { token , id } = data;

        const response = await Vue.axios.get(`/group/${id}`, {
           headers: { "Authorization": token }
        });

        return response.data;
    } catch (e) {
        return e;
    }
}

export async function updateImageGroup({ commit }, Data) {
    try {
        const { data, id, token } = Data;

        const response = await Vue.axios.put(`/group/image-update/${id}`, data, {
            headers: { "Authorization": token }
        });

        return response.data;

    } catch (e) {
        return e;
    }
}

export async function updateNameOfGroup({ commit }, data) {
    try {
        const { id, group, token } = data;

        const response = await Vue.axios.put(`/update-group/${id}`, group, {
            headers: { "Authorization": token  }
        });

        return response.data;

    } catch (e) {
        return e;
    }
}

export async function getGroupMembers({ commit }, data ) {
    try {
        const { id, token } = data;

        const response = await Vue.axios.get(`members/${id}`, {
            headers: { "Authorization": token }
        });

        commit("Groups/setMembers", response.data.data, { root: true });

        return response.data.data;

    } catch (e) {
        return e;
    }
}

export async function addMemberToGroup({ commit }, data) {
    try {
        const { groupId, userId, token } = data;
        const addMember = { groupId, userId };

        const response = await Vue.axios.post("/add-members", addMember, {
            headers: { "Authorization": token }
        });

        return response.data;
    } catch (e) {
        console.log(e);
    }
}

export async function deleteMember({ commit }, data) {
    try {
        const { groupId, userId, token } = data;

        const response = await Vue.axios.delete(`/group/${groupId}/${userId}`, {
            headers: { "Authorization": token }
        });

        console.log(response.data);

        return response.data;

    } catch (e) {
        console.log(e);
    }
}