import Vue from 'vue';

export function setUser(state, token) {
    state.user = Vue.$jwt.decode(token);
}

export function setLogin(state, loggin) {
    state.isLoged = loggin;
}