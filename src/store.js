import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist'

Vue.use(Vuex);

import Auth from './modules/auth/index';
import User from './modules/user/index';
import Groups from "./modules/groups/index";


const vuexPersist = new VuexPersist({
    key: 'token',
    storage: localStorage,
    modules: ["Auth", "User", "Groups"]
});

export default new Vuex.Store({
    modules: {
        Auth,
        User,
        Groups
    },

    plugins: [vuexPersist.plugin]
})
