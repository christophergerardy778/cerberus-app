import Vue from 'vue';
import Router from 'vue-router';
import Login from "./views/auth/Login";
import Groups from "./views/app/groups/Groups";
import GroupPublications from "./views/app/groups/GroupPublications";
import Register from "./views/auth/Register";
import store from "./store";
import JoinAGroup from "./views/app/groups/JoinAGroup";
import Profile from "./views/app/profile/Profile";
import InfoProfile from "./views/app/profile/InfoProfile";

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      meta: {
        requireAuth: true
      }
    },
    {
      path: '/home',
      name: 'home',
      component: Groups,
      meta: {
        requireAuth: true
      }
    },
    {
      path: '/group/:id',
      name: 'group',
      component: GroupPublications,
      meta: {
        requireAuth: true
      }
    },
    {
      path: '/group/request/join/:id',
      name: 'join a group',
      component: JoinAGroup,
      meta: {
        requireAuth: true
      }
    },

    {
      path: '/user/:id',
      name: 'user',
      component: InfoProfile,
      meta: {
        requireAuth: true
      }
    },
  ]
});

router.beforeEach((to, from, next) =>{
  const requiredAuth = to.matched.some(route => route.meta.requireAuth);

  const userLogged = store.state.User.isLoged;

  if (requiredAuth && !userLogged) return next("/");

  if (!requiredAuth && userLogged) return next("/home");

  return next();
});

export default router;
