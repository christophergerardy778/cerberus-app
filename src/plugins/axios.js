import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';


axios.defaults.baseURL = "http://localhost:3000/server/";
//axios.defaults.baseURL = "http://christophergerardy.me/server/";

Vue.use(VueAxios, axios);