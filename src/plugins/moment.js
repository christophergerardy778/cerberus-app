import Vue from 'vue';
import 'moment/locale/es';
import moment from 'moment';

Vue.use(require('vue-moment'), {
    moment
});